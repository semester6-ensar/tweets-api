﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tweets_api.Database;
using tweets_api.Entities;
using tweets_api.Requests;

namespace tweets_api.Repositories
{
    public class TweetsService : ITweetsService
    {
        private TweetsDatabaseContext _context;

        public TweetsService(TweetsDatabaseContext context)
        {
            _context = context;
        }
        public void InsertTweet(Tweet tweet)
        {
            _context.Tweets.Add(tweet);
        }

        public void DeleteTweet(Guid tweetId)
        {
            var tweetToDelete = _context.Tweets.Find(tweetId);
            if(tweetToDelete != null)
            {
                _context.Tweets.Remove(tweetToDelete);
            }            
        }

        public IEnumerable<Tweet> GetTweets()
        {
            return _context.Tweets.ToList();
        }

        public IEnumerable<Tweet> GetTweetsByUserId(Guid userId)
        {
            return _context.Tweets.AsQueryable().Where(tweet => tweet.UserId.Equals(userId));
        }

        public Tweet GetTweetById(Guid tweetId)
        {
            return _context.Tweets.Find(tweetId);
        }

        public Task Save()
        {
           return _context.SaveChangesAsync();
        }

        public void UpdateTweet(Tweet tweet)
        {
            var tweetToUpdate = _context.Tweets.Find(tweet.Id);
            tweetToUpdate.Content = tweet.Content;
            tweetToUpdate.UpdatedAt = DateTime.Now.ToLocalTime();

            _context.Tweets.Update(tweetToUpdate);
        }

        private bool disposed = false;

        public object Tweet => throw new NotImplementedException();

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
