﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using tweets_api.Entities;
using tweets_api.Requests;

namespace tweets_api.Repositories
{
    public interface ITweetsService
    {
        void DeleteTweet(Guid TweetId);
        Tweet GetTweetById(Guid TweetId);
        IEnumerable<Tweet> GetTweets();
        IEnumerable<Tweet> GetTweetsByUserId(Guid userId);
        void InsertTweet(Tweet Tweet);
        Task Save();
        void UpdateTweet(Tweet Tweet);
    }
}