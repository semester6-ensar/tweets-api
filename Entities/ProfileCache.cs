﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tweets_api.Entities
{
    public class ProfileCache
    {
        public string Id { get; set; }
        public string Tag { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
    }
}
