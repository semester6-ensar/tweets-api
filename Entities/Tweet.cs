﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tweets_api.Entities
{
    public class Tweet
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now.ToLocalTime(); 
        public DateTime UpdatedAt { get; set; } = DateTime.Now.ToLocalTime();
    }
}
