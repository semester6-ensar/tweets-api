﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using tweets_api.Database;
using tweets_api.Entities;

namespace tweets_api.Redis
{
    public class ProfileCacheHandler
    {
        private static TweetsDatabaseContext _context;
        public ProfileCacheHandler(TweetsDatabaseContext context)
        {
            _context = context;
        }

        public static void ProfileCreatedHandler(RedisChannel channel, RedisValue message)
        {
            var profile = JsonConvert.DeserializeObject<ProfileCache>(message);
            _context.Profiles.Add(profile);
        }

        public static void ProfileUpdatedHandler(RedisChannel channel, RedisValue message)
        {
            var profile = JsonConvert.DeserializeObject<ProfileCache>(message);
            _context.Profiles.Add(profile);

            var profileToUpdate = _context.Profiles.Find(profile.Id);
            profileToUpdate.Name = profile.Name ?? profileToUpdate.Name;
            profileToUpdate.Tag = profile.Tag ?? profileToUpdate.Tag;
            profileToUpdate.Avatar = profile.Avatar ?? profileToUpdate.Avatar;

            _context.Profiles.Update(profileToUpdate);
        }
    }
}
