﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace tweets_api.Jwt
{
    public static class JwtDecoder
    {
        public static JwtSecurityToken Decode(string authorization)
        {
            var jwt = authorization.Split(' ')[1];
            return new JwtSecurityTokenHandler().ReadJwtToken(jwt);
        }
    }
}
