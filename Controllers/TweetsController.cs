﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tweets_api.Entities;
using tweets_api.Jwt;
using tweets_api.Models;
using tweets_api.Repositories;
using tweets_api.Requests;
using tweets_api.Responses;

namespace tweets_api.Controllers
{
    [ApiController]
    [Route("/api/v1/tweets")]
    public class TweetsController : ControllerBase
    {
        ITweetsService _service;
        public TweetsController(ITweetsService service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<Tweet> GetAllTweets()
        {
            return _service.GetTweets();
        }

        [HttpGet("{id}")]
        public Tweet GetTweetById(Guid id)
        {
            return _service.GetTweetById(id);
        }
        
        [HttpGet("user/{userId}")]
        public IEnumerable<Tweet> GetTweetsByUserId(Guid userId)
        {
            return _service.GetTweetsByUserId(userId);
        }

        [HttpPost]
        public async Task<ActionResult<Tweet>> PostTweet(PostTweetRequest tweet, [FromHeader] string authorization)
        {
            Tweet tweetToAdd = new Tweet();
            tweetToAdd.Content = tweet.Content;
            tweetToAdd.UserId = JwtDecoder.Decode(authorization).Subject;

            _service.InsertTweet(tweetToAdd);
            await _service.Save();

            return Created($"/api/v1/{tweetToAdd.Id}", tweet);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Tweet>> DeleteTweet(Guid id)
        {
            _service.DeleteTweet(id);
            await _service.Save();

            return NoContent();
        }

        [HttpPut]
        public async Task<ActionResult<Tweet>> UpdateTweet(Tweet tweet)
        {
            _service.UpdateTweet(tweet);
            await _service.Save();

            return NoContent();
        }
    }
}
