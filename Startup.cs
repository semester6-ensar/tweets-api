using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tweets_api.Database;
using tweets_api.Entities;
using tweets_api.Jwt;
using tweets_api.Redis;
using tweets_api.Repositories;

namespace tweets_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var redis = ConnectionMultiplexer.Connect("redis");
            SubscribeToProfileTasks(redis);
            services.AddSingleton(redis);
            services.AddDbContextPool<TweetsDatabaseContext>(
                options => options.UseMySql(Configuration.GetConnectionString("MySqlConnectionString")
            ));
            services.AddScoped<ITweetsService, TweetsService>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void SubscribeToProfileTasks(ConnectionMultiplexer redis)
        {
            var sub = redis.GetSubscriber();
            sub.Subscribe("profile:create", (channel, message) => ProfileCacheHandler.ProfileCreatedHandler(channel, message));
            sub.Subscribe("profile:update", (channel, message) => ProfileCacheHandler.ProfileUpdatedHandler(channel, message));
        }
    }
}
