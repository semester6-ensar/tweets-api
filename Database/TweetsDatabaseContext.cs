﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tweets_api.Entities;

namespace tweets_api.Database
{
    public class TweetsDatabaseContext : DbContext
    {
        public DbSet<Tweet> Tweets { get; set; }
        public DbSet<ProfileCache> Profiles { get; set; }

        public TweetsDatabaseContext(DbContextOptions<TweetsDatabaseContext> options) : base(options)
        {

        }
    }
}
